package br.com.projetobrq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class Consumidor {

	@Id
	@Column(name = "idConsumidor")
	private Long idConsumidor;
	
	@Column(name = "cpfConsumidor")
	private String cpfConsumidor;
	
	@Column(name = "rgConsumidor")
	private String rgConsumidor;
	
	@Column(name = "nomeConsumidor")
	private String nomeConsumidor;
	
	@Column(name = "cepConsumidor")
	private String cepConsumidor;
	
	@Column(name = "cidadeConsumidor")
	private String cidadeConsumidor;

	private boolean cepExiste;
	
	public Consumidor() {

	}

	public Long getIdConsumidor() {
		return idConsumidor;
	}

	public void setIdConsumidor(Long idConsumidor) {
		this.idConsumidor = idConsumidor;
	}

	public String getCpfConsumidor() {
		return cpfConsumidor;
	}

	public void setCpfConsumidor(String cpfConsumidor) {
		this.cpfConsumidor = cpfConsumidor;
	}

	public String getRgConsumidor() {
		return rgConsumidor;
	}

	public void setRgConsumidor(String rgConsumidor) {
		this.rgConsumidor = rgConsumidor;
	}

	public String getNomeConsumidor() {
		return nomeConsumidor;
	}

	public void setNomeConsumidor(String nomeConsumidor) {
		this.nomeConsumidor = nomeConsumidor;
	}

	public String getCepConsumidor() {
		return cepConsumidor;
	}

	public void setCepConsumidor(String cepConsumidor) {
		this.cepConsumidor = cepConsumidor;
	}

	public String getCidadeConsumidor() {
		return cidadeConsumidor;
	}

	public void setCidadeConsumidor(String cidadeConsumidor) {
		this.cidadeConsumidor = cidadeConsumidor;
	}

	public boolean isCepExiste() {
		return cepExiste;
	}

	public void setCepExiste(boolean cepExiste) {
		this.cepExiste = cepExiste;
	}

}
