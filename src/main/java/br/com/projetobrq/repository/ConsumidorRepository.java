package br.com.projetobrq.repository;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.projetobrq.model.Consumidor;

@Repository
public interface ConsumidorRepository extends CrudRepository<Consumidor, Long>  {
	
	@Query("Select u from Consumidor u where u.idConsumidor=:idConsumidor")
	Consumidor findByIdConsumidor(@Param("idConsumidor") long idConsumidor);
	
	List<Consumidor> findAll();

	void deleteAll();
	
}
