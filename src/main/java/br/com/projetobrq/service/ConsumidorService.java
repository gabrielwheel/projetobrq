package br.com.projetobrq.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projetobrq.model.Consumidor;
import br.com.projetobrq.repository.ConsumidorRepository;

@Service
public class ConsumidorService {
	
	@Autowired
	ConsumidorRepository repository;

	public Consumidor saveOrUpdate(Consumidor consumidor) {
		return repository.save(consumidor);
	}

	public Consumidor findByIdConsumidor(long id) {
		return repository.findByIdConsumidor(id);
	}
	
	public List<Consumidor> findAll() {
		return repository.findAll();
	}

	public void deleteAll() {
		repository.deleteAll();
	}

}
