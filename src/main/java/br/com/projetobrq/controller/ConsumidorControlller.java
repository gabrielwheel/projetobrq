package br.com.projetobrq.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import br.com.projetobrq.model.Consumidor;
import br.com.projetobrq.service.ConsumidorService;

@Controller
public class ConsumidorControlller {

	@Autowired
	ConsumidorService consumidorService;
	
	@GetMapping("/adicionaConsumidor")
	public String adicionaConsumidor(Model model) {
		
		Consumidor consumidor = new Consumidor();
		List<Consumidor> consumidores = consumidorService.findAll();
		
		model.addAttribute("consumidor", consumidor);
		if(consumidores != null && consumidores.size()>0) {
			model.addAttribute("existeListaConsumidor", true);
			model.addAttribute("consumidores", consumidores);
		}

		return "adicionaConsumidorView";
	}
	
	@PostMapping("/adicionaConsumidor")
	public RedirectView adicionaConsumidorRedirect(@ModelAttribute("consumidor") Consumidor consumidor, RedirectAttributes redirectAttributes) throws IOException, ParseException {
		final RedirectView redirectView = new RedirectView("/adicionaConsumidor", true);	
		
		Long id = (long) (consumidorService.findAll().size() + 1);
		consumidor.setIdConsumidor(id);
		
		consumidor = viaCepRequisicao(consumidor);
		
		if (consumidor.getCidadeConsumidor()!= null) {
			Consumidor consumidorNovo = consumidorService.saveOrUpdate(consumidor);
			
			redirectAttributes.addFlashAttribute("consumidor", consumidorNovo);
	        redirectAttributes.addFlashAttribute("consumidorSuccess", true);
	        redirectAttributes.addFlashAttribute("consumidorSuccessID", id);
		} else {
			redirectAttributes.addFlashAttribute("consumidorFalhou", true);
			redirectAttributes.addFlashAttribute("consumidorCEP", consumidor.getCepConsumidor());
		}
		
		
		return redirectView;
		
	}
	
	public Consumidor viaCepRequisicao(Consumidor consumidor) throws IOException, ParseException {
		URL url = new URL("http://viacep.com.br/ws/"+ consumidor.getCepConsumidor().replace("-", "") +"/json");
        URLConnection urlConnection = url.openConnection();
        InputStream is = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        
        JSONObject jsonMaster = (JSONObject) new JSONParser().parse(br);
        
        if(jsonMaster.get("erro") != null && jsonMaster.get("erro").equals(true)) {
        	consumidor.setCepExiste(false);
        } else {
        	consumidor.setCidadeConsumidor(jsonMaster.get("localidade").toString());
        	consumidor.setCepExiste(true);
        }
        
        return consumidor;
	}
	
}
