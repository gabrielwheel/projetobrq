<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

<html>
<head>
<title>Adicionar Consumidor</title>
</head>
<body>
<c:if test="${existeListaConsumidor}">
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>CPF</th>
				<th>RG</th>
				<th>CEP</th>
				<th>Cidade</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${consumidores}" var="consItem">
				<tr>
				<td>${consItem.idConsumidor}</td>
				<td>${consItem.cpfConsumidor}</td>
				<td>${consItem.rgConsumidor}</td>
				<td>${consItem.nomeConsumidor}</td>
				<td>${consItem.cepConsumidor}</td>
				<td>${consItem.cidadeConsumidor}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

	<c:if test="${consumidorSuccess}">
		<div>Consumidor salvo, ID: ${consumidorSuccessID}</div>
	</c:if>
	<c:if test="${consumidorFalhou}">
		<div>CEP não existente: ${consumidorCEP}</div>
	</c:if>
	

	<c:url var="addConsumidor" value="/adicionaConsumidor" />
	<form:form action="${addConsumidor}" method="post"
		modelAttribute="consumidor">
		<form:label path="cpfConsumidor">CPF: </form:label>
		<form:input class="cpf" type="text" path="cpfConsumidor"
			id="cpfConsumidor" />
		<br>
		<br>
		<form:label path="rgConsumidor">RG: </form:label>
		<form:input class="rg" type="text" path="rgConsumidor" />
		<br>
		<br>
		<form:label path="nomeConsumidor">Nome Completo: </form:label>
		<form:input path="nomeConsumidor" />
		<br>
		<br>
		<form:label path="cepConsumidor">CEP: </form:label>
		<form:input class="cep" path="cepConsumidor" />
		<br>
		<br>
		<input type="submit" value="Enviar!" />
	</form:form>
</body>
<script>
	$('.cpf').mask('000.000.000-00');
	$('.rg').mask("99.999.999-A");
	$('.cep').mask('00000-000');
</script>
</html>